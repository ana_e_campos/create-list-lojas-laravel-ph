@extends('layouts.main')

@section('title', 'Home')

@section('content')

    <div id="search-container" class="col-md-12">
        <h1>Busque uma loja</h1>
        <form action="">
            <input type="text" id="search" name="search" class="form-control">
        </form>
    </div>
    <div id="lojas-container" class="col-md-12">
        <h2>Lojas</h2>
        <p>Veja todas as nossas lojas</p>
        <div id="cards-container" class="row">
            @foreach ($lojas as $loja)
                <div class="col-md-3" id="loja">
                    <div class="card">
                        <img src="/img/loja/{{ $loja->imagem }}" class="card-img-top" alt="{{ $loja->nome }}">
                        <div class="car-body">
                            <h5 class="card-title">{{ $loja->nome }}</h5>
                            <p class="card-dados">{{ $loja->telefone }}</p>
                            <a href="/lojas/{{ $loja->id }}" class="btn btn-danger">Saiba mais</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    
 <!--comentário aparece no console-->
 {{-- este comentário não aparece no Blade --}}
@endsection