@extends('layouts.main')

@section('title', 'Detalhe')

@section('content')

 <div id="detalhe-loja" class="col-md-10 offset-md-1">
    <div class="row">
        <div id="image-container" class="col-md-6">
            <img src="/img/loja/{{ $loja->imagem }}" class="img-fluid" alt="{{ $loja->nome }}">
        </div>
        <div id="info-container" class="col-md-6">
            <h1>{{ $loja->nome }}</h1>
            <p class="loja-endereco"><ion-icon name="location-outline"></ion-icon> Endereço: {{ $loja->endereco }}</p>
            <p class="loja-telefone"><ion-icon name="call-outline"></ion-icon> Telefone: {{ $loja->telefone }}</p>
            <p class="loja-gerente"><ion-icon name="person-outline"></ion-icon> Gerente: {{ $loja->gerente }}</p>
        </div>
    </div>
 </div>

@endsection