<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegiaoController extends Controller
{
    public function index(){
    
        $nomes = ['Grande Florianópolis', 'Vale do Itajaí', 'Norte'];
    
        return view('welcome', 
        [
            'nome' => $nomes
        ]);
    }
    
    public function create(){
        return view('regiao.create');
    }
}
