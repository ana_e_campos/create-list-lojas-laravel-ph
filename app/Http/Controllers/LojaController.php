<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Loja;

class LojaController extends Controller
{
    public function index(){
        $lojas = Loja::all();
        return view('welcome', ['lojas' => $lojas]);
    }
    public function lojas(){
        $lojas = Loja::all();
        return view('loja', ['lojas' => $lojas]);
    }
    public function create(){
        return view('loja.create');
    }
    public function store(Request $request){
        $loja = new Loja;
        $loja->nome = $request->nome;
        $loja->filial = $request->filial;
        $loja->endereco = $request->endereco;
        $loja->telefone = $request->telefone;
        $loja->gerente = $request->gerente;

        //image upload
        if($request->hasFile('imagem') && $request->file('imagem')->isValid()){
            $requestImage = $request->imagem;
            $extension = $requestImage->extension();
            $imageName = md5($requestImage->getClientOriginalName() . strtotime("now")) . "." . $extension;
            $request->imagem->move(public_path('img/loja'), $imageName);
            $loja->imagem = $imageName;
        }

        $loja->save();
        return redirect('/')->with('msg', 'Loja criada com sucesso!');
    }

    public function show($id){
        $loja = Loja::findOrFail($id);
        return view('loja.show', ['loja' => $loja]);
    }
}